using System;
using System.Collections.Generic;

namespace design_patterns
{
    class CPU : ICloneable
    {
        private static Dictionary<string, CPU> cpus = new Dictionary<string, CPU>();
        private readonly BrandName brand;
        private readonly int clock;
        public BrandName Brand { get => brand; }
        public int Clock { get => clock; }

        public CPU(BrandName brand, int clock)
        {
            this.brand = brand;
            this.clock = clock;
        }

        public CPU(CPU origin)
        {
            this.brand = origin.brand;
            this.clock = origin.clock;
        }


        public object Clone()
        {
            return this.MemberwiseClone();
        }

        public override string ToString()
        {
            return String.Format("{0}Ghz {1} CPU", Clock, Brand);
        }

        public static CPU buildCPU(BrandName brand, int clock)
        {
            string key = String.Format("{0}{1}", brand, clock);
            if (!cpus.ContainsKey(key))
            {
                cpus.Add(key, new CPU(brand, clock));
            }
            return (CPU)cpus[key].Clone();
        }
    }
}

