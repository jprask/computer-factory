using System;
using System.Collections.Generic;


namespace design_patterns
{
    class Monitor : ICloneable
    {
        private static Dictionary<string, Monitor> monitors = new Dictionary<string, Monitor>();
        private readonly int inches;
        private readonly bool widescreen;
        public int Inches { get => inches; }
        public bool WideScreen { get => widescreen; }

        public Monitor(int inches, bool widescreen)
        {
            this.inches = inches;
            this.widescreen = widescreen;
        }

        public Monitor(Monitor origin)
        {
            this.inches = origin.inches;
            this.widescreen = origin.widescreen;
        }


        public object Clone()
        {
            return this.MemberwiseClone();
        }

        public override string ToString()
        {
            string monitorType = (WideScreen) ? "WideScreen" : "regular";
            return String.Format("{0}\" {1} Monitor", Inches, monitorType);
        }

        public static Monitor buildMonitor(int inches, bool widescreen)
        {
            string key = String.Format("{0}{1}", inches, widescreen);
            if (!monitors.ContainsKey(key))
            {
                monitors.Add(key, new Monitor(inches, widescreen));
            }
            return (Monitor)monitors[key].Clone();
        }
    }
}

