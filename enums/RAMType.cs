using System;

namespace design_patterns
{
    enum RAMType
    {
        TWO_GB,
        FOUR_GB,
        EIGHT_GB,
        SIXTEEN_GB
    }
}
