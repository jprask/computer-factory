# Design Patterns Assignment

This project implements a computer factory using design patterns. Made for an assignment for the design patterns course at UFN.

## Implementation

In this implementation we use the singleton patter for the factory as well as the builder pattern for the assembly line. The parts implement a Prototype pattern for fast dispatch of objects. The factory caches created parts in memory

LINKS https://docs.microsoft.com/en-us/dotnet/api/system.object.memberwiseclone?view=netframework-4.7.2

https://docs.microsoft.com/en-us/dotnet/api/system.collections.hashtable?view=netframework-4.7.2

## Classes

This are the classes used in the project as requested in the assignement. The implementation attributes must be the same as the requested attributes, methods must be implemented according to the chosen patterns.

### __Factory__

The factory must be a singleton. Builds the computers. 

 - Name: string

In this implementation, the factory uses the Builder pattern. The build steps are:

 - setCPU
 - setMonitor
 - buildAllInOne
 - buildNotebook
 - getComputer

### __Computer__

Represens a computer.

 - Model: string
 - RAM: RAMType
 - CPU: CPU
 - Monitor: Monitor

#### __Subclasses__

 - AllInOne
 - Notebook

### __CPU__

Represents a CPU.

 - Brand: BrandName
 - Clock: integer

### __Monitor__

Represents a Monitor

 - Inches: smallint
 - WideScreen: bool

### __Enums__

 - RAMType {2GB, 4GB, 8GB, 16GB}
 - BrandName {Intel, AMD}
