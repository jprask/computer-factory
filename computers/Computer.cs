using System;

namespace design_patterns
{
    abstract class Computer : ICloneable
    {
        protected readonly CPU cpu;
        protected readonly Monitor monitor;
        protected readonly RAMType ram;
        protected readonly string model;
        public CPU Cpu { get => cpu; }
        public Monitor Monitor { get => monitor; }
        public RAMType Ram { get => ram; }
        public string Model { get => model; }

        public Computer(CPU cpu, Monitor monitor, RAMType ram, string model)
        {
            this.cpu = cpu;
            this.monitor = monitor;
            this.ram = ram;
            this.model = model;
        }

        public abstract object Clone();
    }
}

