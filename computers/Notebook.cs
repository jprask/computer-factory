using System;
using System.Collections.Generic;

namespace design_patterns
{
    class Notebook : Computer
    {
        private static CPU manufactureCpu;
        private static Monitor manufactureMonitor;
        private static Dictionary<string, Computer>
            nbs = new Dictionary<string, Computer>();

        public Notebook(CPU cpu, Monitor monitor, RAMType ram, string model) :
            base(cpu, monitor, ram, model)
        { }

        public override Object Clone() { return this.MemberwiseClone(); }

        public override string ToString()
        {
            return String.Format(
                "Notebook {0} computer: {1} {2} {3}",
                this.Model,
                this.Cpu.ToString(),
                this.Ram,
                this.Monitor.ToString()
            );
        }

        static public void setCPU(BrandName brand, int clock)
        {
            manufactureCpu = CPU.buildCPU(brand, clock);
        }

        static public void setMonitor(int inches, bool widescreen)
        {
            manufactureMonitor = Monitor.buildMonitor(inches, widescreen);
        }

        static public Notebook buildNotebook(RAMType ram, string model)
        {
            string key = String.Format(
                "all-in-one{0}{1}{2}{3}",
                model,
                ram,
                manufactureCpu,
                manufactureMonitor
            );
            if (!nbs.ContainsKey(key))
            {
                nbs.Add(
                    key,
                    new Notebook(
                        manufactureCpu,
                        manufactureMonitor,
                        ram,
                        model
                    )
                );
            }
            return (Notebook)nbs[key];
        }
    }
}

