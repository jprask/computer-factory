using System;
using System.Collections.Generic;

namespace design_patterns
{
    class AllInOne : Computer
    {
        private static CPU manufactureCpu;
        private static Monitor manufactureMonitor;
        private static Dictionary<string, Computer>
            aios = new Dictionary<string, Computer>();

        public AllInOne(CPU cpu, Monitor monitor, RAMType ram, string model) :
            base(cpu, monitor, ram, model)
        { }

        public override Object Clone() { return this.MemberwiseClone(); }

        public override string ToString()
        {
            return String.Format(
                "AllInOne {0} computer: {1} {2} {3}",
                this.Model,
                this.Cpu.ToString(),
                this.Ram,
                this.Monitor.ToString()
            );
        }

        static public void setCPU(BrandName brand, int clock)
        {
            manufactureCpu = CPU.buildCPU(brand, clock);
        }

        static public void setMonitor(int inches, bool widescreen)
        {
            manufactureMonitor = Monitor.buildMonitor(inches, widescreen);
        }

        static public AllInOne buildAllInOne(RAMType ram, string model)
        {
            string key = String.Format(
                "all-in-one{0}{1}{2}{3}",
                model,
                ram,
                manufactureCpu,
                manufactureMonitor
            );
            if (!aios.ContainsKey(key))
            {
                aios.Add(
                    key,
                    new AllInOne(
                        manufactureCpu,
                        manufactureMonitor,
                        ram,
                        model
                    )
                );
            }
            return (AllInOne)aios[key];
        }
    }
}

