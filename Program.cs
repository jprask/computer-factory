﻿using System;

namespace design_patterns
{
    class Program
    {
        static void Main(string[] args)
        {
            ComputerFactory factory = ComputerFactoryImpl.Instance;
            Notebook.setCPU(BrandName.AMD, 32);
            Notebook.setMonitor(1080, true);
            Notebook nb = Notebook.buildNotebook(RAMType.EIGHT_GB, "Ultrabook");
            Console.WriteLine(nb.ToString());
        }
    }
}

