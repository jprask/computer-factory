using System;


namespace design_patterns
{
    sealed class ComputerFactoryImpl : ComputerFactory
    {
        private static readonly Lazy<ComputerFactoryImpl> lazy = new Lazy<ComputerFactoryImpl>(
            () => new ComputerFactoryImpl()
        );
        public static ComputerFactoryImpl Instance { get => lazy.Value; }
    }
}

